using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public interface ICustomerService
    {
        Task<IEnumerable<CustomerShortResponse>> GetAllAsync();
        Task<CustomerResponse> GetByIdAsync(Guid id);
        Task<CustomerResponse> CreateAsync(CreateOrEditCustomerRequest request);
        Task<bool> EditAsync(Guid id, CreateOrEditCustomerRequest request);
        Task<bool> DeleteAsync(Guid id);
    }
}