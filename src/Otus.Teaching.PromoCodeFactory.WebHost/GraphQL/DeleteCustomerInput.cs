using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public record DeleteCustomerInput
    (
        Guid Id
    );
}