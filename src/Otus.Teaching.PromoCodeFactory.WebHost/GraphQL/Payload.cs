using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public abstract class Payload
    {
        protected Payload(IReadOnlyList<UserError> errors = null)
        {
            Errors = errors;
        }

        public IReadOnlyList<UserError> Errors { get; }
    }
}