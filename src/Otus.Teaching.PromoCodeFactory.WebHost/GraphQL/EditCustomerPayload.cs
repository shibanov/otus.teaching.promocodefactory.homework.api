using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class EditCustomerPayload : Payload
    {
        public EditCustomerPayload(Customer customer)
        {
            Customer = customer;
        }

        public EditCustomerPayload(UserError error)
            : base(new[] {error})
        {
        }
        
        public Customer Customer { get; }
    }
}