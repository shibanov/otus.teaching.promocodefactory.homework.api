namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class DeleteCustomerPayload : Payload
    {
        public DeleteCustomerPayload(bool result)
        {
            Result = result;
        }

        public DeleteCustomerPayload(UserError error)
            : base(new[] {error})
        {
        }
        
        public bool Result { get; }
    }
}