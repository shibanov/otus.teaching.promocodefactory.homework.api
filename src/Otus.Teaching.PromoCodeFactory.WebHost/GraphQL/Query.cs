using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HotChocolate;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class Query
    {
        public IQueryable<Customer> GetCustomers([Service] DataContext context) =>
            context.Customers;

        public async Task<Customer> GetCustomerByIdAsync(
            Guid id,
            CustomerByIdDataLoader customerById,
            CancellationToken cancellationToken) =>
            await customerById.LoadAsync(id, cancellationToken);

        public async Task<IEnumerable<Customer>> GetCustomersByIdAsync(
            Guid[] ids,
            CustomerByIdDataLoader customerById,
            CancellationToken cancellationToken) =>
            await customerById.LoadAsync(ids, cancellationToken);
    }
}