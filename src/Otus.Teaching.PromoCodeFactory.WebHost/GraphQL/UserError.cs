namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public record UserError(string Message, string Code);
}