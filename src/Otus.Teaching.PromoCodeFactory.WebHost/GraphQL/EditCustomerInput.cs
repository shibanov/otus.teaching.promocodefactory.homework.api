using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public record EditCustomerInput
    (
        Guid Id,
        string FirstName,
        string LastName,
        string Email,
        List<Guid> PreferenceIds
    );
}