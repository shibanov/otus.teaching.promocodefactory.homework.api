using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class CreateCustomerPayload : Payload
    {
        public CreateCustomerPayload(Customer customer)
        {
            Customer = customer;
        }

        public CreateCustomerPayload(UserError error)
            : base(new[] {error})
        {
        }

        public Customer Customer { get; }
    }
}