using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Grpc
{
    public class CustomerGrpcService : Customers.CustomersBase
    {
        private readonly ICustomerService _customerService;

        public CustomerGrpcService(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public override async Task<GetAllResponse> GetAll(Empty request, ServerCallContext context)
        {
            var customers = await _customerService.GetAllAsync();
            var response = new GetAllResponse();
            response.List.AddRange(customers.Select(CustomerMapper.MapFromModel));
            return response;
        }

        public override async Task<CustomerResponse> GetById(IdRequest request, ServerCallContext context)
        {
            Guid id;
            if (!Guid.TryParse(request.Id, out id))
                throw new RpcException(Status.DefaultCancelled, 
                    $"Wrong identifier '{request.Id}'.");

            var customer = await _customerService.GetByIdAsync(id);
            if (customer is null)
                throw new RpcException(Status.DefaultCancelled, 
                    $"Customer by identifier '{id}' not found.");

            return CustomerMapper.MapFromModel(customer);
        }

        public override async Task<CustomerResponse> Create(CreateCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerService.CreateAsync(
                CustomerMapper.MapToModel(request.Payload));

            return CustomerMapper.MapFromModel(customer);
        }

        public override async Task<Empty> Edit(EditCustomerRequest request, ServerCallContext context)
        {
            Guid id;
            if (!Guid.TryParse(request.Id, out id))
                throw new RpcException(Status.DefaultCancelled, 
                    $"Wrong identifier '{request.Id}'.");

            var result = await _customerService.EditAsync(id, CustomerMapper.MapToModel(request.Payload));
            if (!result)
                throw new RpcException(Status.DefaultCancelled, 
                    $"Customer by identifier '{id}' not found.");
            
            return new Empty();
        }

        public override async Task<Empty> Delete(IdRequest request, ServerCallContext context)
        {
            Guid id;
            if (!Guid.TryParse(request.Id, out id))
                throw new RpcException(Status.DefaultCancelled, 
                    $"Wrong identifier '{request.Id}'.");

            var result = await _customerService.DeleteAsync(id);
            if (!result)
                throw new RpcException(Status.DefaultCancelled, 
                    $"Customer by identifier '{id}' not found.");
            
            return new Empty();
        }
    }
}