using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GreenDonut;
using HotChocolate.DataLoader;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class CustomerByIdDataLoader : BatchDataLoader<Guid, Customer>
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomerByIdDataLoader(
            IBatchScheduler batchScheduler,
            IRepository<Customer> customerRepository)
            : base(batchScheduler)
        {
            _customerRepository = customerRepository;
        }

        protected override async Task<IReadOnlyDictionary<Guid, Customer>> LoadBatchAsync(
            IReadOnlyList<Guid> keys,
            CancellationToken cancellationToken)
        {
            var customers = await _customerRepository.GetRangeByIdsAsync(keys.ToList());
            return customers.ToDictionary(x => x.Id);
        }
    }
}